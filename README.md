# README #

This README covers bib file and reference management in edhec-infra projects. 

## tags 
tags are used with biber to generate bibliographies automatically organised by categories

* infra: for edhec_infra publications, presentations, etc
* press: articles published in the media written by us
* edhec: any ERI, EDHEC, EDHEC infra publication
* reviewed: anything published officially 
* talk: presentation of any kind
* indus: talks to non-academic audiences

## adding the master bib file to a paper

in terminal, cd to paper git repo and 
```
#!git
git submodule add https://bitbucket.org/edhecinfra/bib bib
```
in the paper with natbib use
```
\bibliography{bib/library}
```
with biblatex
```
\addbibresource{./bib/library.bib}
```
or 
```
\begin{refsection}[./bib/library]
   \nocite{*}   
   \printbibliography[type=inproceedings, keyword={talk}, title={Academic Presentations}]
\end{refsection}
```